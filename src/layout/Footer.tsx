import React, { Component } from 'react';

function Footer() {
   
        return (
            <React.Fragment>
                <footer>
                    <div className="footer-area">
                        <p>&copy; Copyright {new Date().getFullYear()}. All right reserved. Template by Raventhemez.</p>
                    </div>
                </footer>
            </React.Fragment>
        );
    
}

export default Footer



